
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */


def static "newKeyword.scrollUpDown.updown"(
    	int startX	
     , 	int startY	
     , 	int endX	
     , 	int endY	) {
    (new newKeyword.scrollUpDown()).updown(
        	startX
         , 	startY
         , 	endX
         , 	endY)
}
