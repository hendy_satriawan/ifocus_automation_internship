package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object appName
     
    /**
     * <p></p>
     */
    public static Object count
     
    /**
     * <p></p>
     */
    public static Object isOffline
     
    /**
     * <p></p>
     */
    public static Object planDoctor
     
    /**
     * <p></p>
     */
    public static Object planNonDoctor
     
    /**
     * <p></p>
     */
    public static Object tidakDiisi
     
    /**
     * <p></p>
     */
    public static Object tglLain
     
    /**
     * <p></p>
     */
    public static Object MasterDoctor
     
    /**
     * <p></p>
     */
    public static Object MasterNonDoctor
     
    /**
     * <p></p>
     */
    public static Object UnplanDoctor
     
    /**
     * <p></p>
     */
    public static Object UnplanNonDoctor
     
    /**
     * <p>Profile JK3VIC0103 : ambil yg 114534 (paling bawah)
Profile PWKVIC0103 : ambil yg 114534 (paling bawah)</p>
     */
    public static Object UnplanOutlet
     
    /**
     * <p></p>
     */
    public static Object outletPIC
     
    /**
     * <p></p>
     */
    public static Object Username
     
    /**
     * <p></p>
     */
    public static Object Password
     
    /**
     * <p></p>
     */
    public static Object Username2
     
    /**
     * <p></p>
     */
    public static Object Password2
     
    /**
     * <p></p>
     */
    public static Object Bawahan
     
    /**
     * <p></p>
     */
    public static Object draft
     
    /**
     * <p></p>
     */
    public static Object syncStatus
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            appName = selectedVariables['appName']
            count = selectedVariables['count']
            isOffline = selectedVariables['isOffline']
            planDoctor = selectedVariables['planDoctor']
            planNonDoctor = selectedVariables['planNonDoctor']
            tidakDiisi = selectedVariables['tidakDiisi']
            tglLain = selectedVariables['tglLain']
            MasterDoctor = selectedVariables['MasterDoctor']
            MasterNonDoctor = selectedVariables['MasterNonDoctor']
            UnplanDoctor = selectedVariables['UnplanDoctor']
            UnplanNonDoctor = selectedVariables['UnplanNonDoctor']
            UnplanOutlet = selectedVariables['UnplanOutlet']
            outletPIC = selectedVariables['outletPIC']
            Username = selectedVariables['Username']
            Password = selectedVariables['Password']
            Username2 = selectedVariables['Username2']
            Password2 = selectedVariables['Password2']
            Bawahan = selectedVariables['Bawahan']
            draft = selectedVariables['draft']
            syncStatus = selectedVariables['syncStatus']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
