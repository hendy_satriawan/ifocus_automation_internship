<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>eksplor test suit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>91bce3ac-5159-45b2-b24c-0880193b33b6</testSuiteGuid>
   <testCaseLink>
      <guid>daaaa0fd-e912-463a-b22c-c735afe36bff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.1 Eksplore - buka menu explore dan cek list menu yang muncul</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81f8449a-b63a-44bf-9353-129c878611e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.2 Journal - View list menu journal pada tab terbaru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c026e3f1-32a3-41cb-bd5a-b0cb8f79f938</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.3 Journal - share journal ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41280140-d6a4-4676-a15a-65a12ab7442b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.4 Journal - share journal ke Team (aplikasi Ifocus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8f41d1c-68a6-4016-bdb1-d9b238813eb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.5 Journal - batal share dengan pilih CLOSE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b990799f-6b9f-4134-86bd-509ffe8ee18b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.6 Journal - Bookmark Journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68416bb3-7c7b-4bb9-a96f-470cb415dacf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.7 Journal - unbookmark journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df7f0759-8fba-44a0-9948-babb057e13e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.8 Journal - View detail journal pada tab terbaru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>649eee65-a681-4947-be3e-1530d7afc93a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.9 Journal - klik icon Nav Back untuk kembali ke list journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6db51f1-b931-4257-b444-1e5925360794</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.10 Journal - klik icon X setelah input keywords pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a6f4129-a6af-460d-a8ba-ccb01954c316</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.11 Journal - Pilih Batal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbc6e8db-baa1-447c-94d1-fbf7bbaa73b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.12 Journal - pilih Terapkan tanpa pilih produk yang difilter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eefe9ea9-ee45-47b6-8286-0261a064c353</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/1.13 Journal - Filter Journal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ece4039b-4a69-4d5a-b6eb-37b035519c6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.1 Video - View list video pada tab terbaru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c183257-b164-429b-96ec-5569e7ad99ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.2 Video - Share video ke Dokter (D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e03226e2-2e0e-4a08-a616-8c7a378630d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.3 Video - Share video ke Team (iFocus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dfdaf44-3550-47e6-b517-080667c263ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.4 Video - Batal share dengan pilih CLOSE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ec32692-fdc6-4794-b044-5ed59cd6042e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.5 Video - bookmark video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d177548f-536f-4a35-aee9-24b930becf22</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.6 Video - unbookmark video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9dbc854-1975-4792-90e0-e11e61d8c60d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.7 Video - View detail video pada tab terbaru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4551478e-51a9-4f76-a73c-d1bdb661633c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.8 Video - klik icon Nav Back untuk kembali ke list video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>149028fa-1ec4-4da4-8628-44cdcb7e25ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.9 Video - pilih tab tersimpan, check video yang dibookmark di tab tersimpan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6396a10d-c436-4d9d-992e-4d89b544d271</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.10 Video - klik icon X setelah input keywords pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55241c16-a255-4d60-b5f6-6e0ae4c0e285</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.11 Video - klik icon Nav Back pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5729a469-7a8f-4490-8fe1-123369229d99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.12 Video - pilih BATAL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8337d61f-d976-4420-9409-91c679b7ce62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.13 Video- pilih TERAPKAN tanpa pilih produk yang difilter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37a8db69-0ce9-4d3b-a945-75796c9f5bd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/2.14 Video - filter video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d72a80b3-bc4f-4753-848b-25faa11c700c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.1 Product Knowladge - melihat daftar produk di menu product knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b6f4d085-ba6f-4f95-a85c-e4c087450f1a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.2 Product Knowladge - search product di product knowledge (hanya bisa menampilkan pencarian product knowledge)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a752827-28fe-4ca5-8ef6-c3cd3d742333</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.3 Product Knowladge - View konten product knowledge pada tab brochure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ad6027b-9c22-4b1b-afd0-1f990a095b20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.4 Tab Brosur - open dan download PDF pada tab brochure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25ecbcd1-7853-438a-b34f-d1785153f668</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.5 Tab Brosur - share product knowledge - brochure ke Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f358b78f-a50d-441a-8871-1cba71e96a59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.6 Tab Brosur - share product knowledge - brochure ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bbc6190-ab6e-418b-a847-0ec249a564c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.7 Tab Brosur - bookmark product knowledge - brochure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb990855-eaa4-449f-94c3-f09e0e8bc793</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/3.8 Tab Brosur - unbookmark product knowledge - brochure</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d91d256-89f5-4b8f-9924-ee4a23469c24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.1 Tab Knowladge - View konten product knowledge pada tab knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52cdf917-7243-4851-8cce-177fa908af57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.2 Tab Knowladge - open dan download PDF pada tab knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>917c025f-3c15-464e-98a6-83b4de89e61f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.3 Tab Knowladge - share product knowledge ke Team (aplikasi Ifocus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f45769fe-75fc-4de4-9516-321eb56afa0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.4 Tab Knowladge - share product knowledge ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7a47c60-7109-4fc1-a5b2-9ca93f2ea553</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.5 Tab Knowledge - Bookmark product knowledge - knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ae0813b-8d1f-4e83-9c50-9b8b39435fb5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.6 Tab Knowledge - unbookmark product knowledge - knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5b0ce20-b878-49c6-acd8-a1360d80cfdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.7 Tab Knowledge - View konten product knowledge pada tab video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>839ec56a-57c2-416b-80ac-3e4fcce49857</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/4.8 Tab Knowledge - download video untuk diputar offline</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98f66956-e12d-4a7c-9200-202ff34a86ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.1 Tab Video - View konten product knowledge pada tab video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a19b896-b4ad-44bd-bb39-b4d5e5aa255b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.2 Tab Video - download video pada tab video untuk diputar offline</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8572a6c6-7188-441b-98ce-44004d8c1070</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.3 Tab Video - share product knowledge ke Team (aplikasi Ifocus)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>863b74d5-da7d-46ea-a505-8b95d9b8d35c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.4 Tab Video - share product knowledge ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f046eb65-fc08-496e-a523-273d5649fcda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.5 Tab Video - Bookmark product knowledge - Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc11a8f7-665f-4510-96ac-fa9c79101433</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/5.6 Tab Video - unbookmark product knowledge - Video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86d675be-9a17-47f9-bdb8-905b79da9d9e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.1 Tab Literatur- View konten product knowledge - Literatur</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>628c3515-1218-49b6-8b11-f1f087f2783d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.2 Tab Literatur - open dan download PDF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b82758d-1f68-465c-9779-20b0e0a43184</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.3 Tab Literatur- share product knowledge ke Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e76e8dde-7017-49bc-8702-1414a9d9f379</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.4 Tab Literatur - share product knowledge ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9426c672-ad3d-4eaf-972f-8b757cd438af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.5 Tab Literatur - Bookmark product knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c6c4658-5d2e-4154-ad95-7c8ed1d55929</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/6.6 Tab Literatur - unbookmark product knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9315615e-9252-4484-95c7-7ecd28ae0921</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/7.1 Tab Deskripsi - View konten product knowledge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3415ba6-7399-4cc5-8b37-8ab74668c81e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/7.2 Tab Deskripsi - open dan download PDF</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b792ae7-5729-4051-a737-196ea24ec0d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/7.3 Tab Deskripsi - share product knowledge ke Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f7d9f31-165c-43c6-b499-74de6ca188e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/7.4 Tab Deskripsi - share product knowledge ke Dokter (aplikasi D2D)</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
