<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>call plan test suit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8597906a-b13a-4a67-98d0-6b37b401c490</testSuiteGuid>
   <testCaseLink>
      <guid>85a7b36c-e4ac-45b0-a8a5-1a8a4596524b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/1.1 Synch - MR melakukan proses synchronization agar data sama antara db local dan server</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>828084b1-0f8b-4152-ab87-b1ce8395a07e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.1 Overview - MR membuat call plan dari menu call overview dengan tipe customer Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac082a59-c72d-406a-8682-c5c0fa864a64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.2 Overview - MR membuat call plan dari menu call overview dengan tipe customer Non-Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a38c688-0120-4af6-afa1-4f80d584146e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.3 Overview - MR membuat call plan dengan tidak isi nama dokter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0dcc86d-b070-4933-9281-bcc692f77b83</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.4 Overview - MR membuat call plan dengan tidak isi status call plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09bcbfd1-327a-4aec-a81b-ac8693435ae7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.5 Overview - MR membuat call plan dengan tidak isi channel call plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a69e34c0-ec5b-4fee-9846-77022fb7e146</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.6 Overview - MR membuat call plan dengan activities tidak isi activites</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>238ad24d-01a3-4c8b-a59e-dcc49dd8175d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.7 Overview - MR membuat call plan tidak isi date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbba1270-98e7-4070-a633-e26ace7d7d00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/2.8 Overview - MR membuat call plan dengan lebih dari 1 tanggal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e793d9d3-90a1-47a9-9eb2-4b391eafe652</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/3.1 Masterlist - MR membuat call plan dari menu masterlist dengan tipe customer Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66f1aa25-a294-48b9-b919-71a56674e19c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/3.2 Masterlist - MR membuat call plan dari menu masterlist dengan tipe customer Non-Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9272992-11c4-4050-8cba-2e1923337331</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/4.1 Edit - MR melakukan edit call plan status New request sebelum synch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>695c8094-1f83-4085-be31-605ad6aacfce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/4.2 Edit - MR melakukan edit call plan status New Request Synch setelah synch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2f7c6fc-ba92-4a71-8aae-1beba2bd1055</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/5.1 Delete - MR Melakukan delete salah satu call plan status New Request sebelum sync</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56852438-2826-4dbf-af33-e44504a0b0c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Call Plan Management/5.2 Delete - MR Melakukan delete salah satu call plan status New Request Synch setelah sync</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c473292-2c4c-4c4d-8f17-71083fbee55c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/6.1 Add Unplan - MR membuat Unplan dengan tipe customer Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7334c6f3-88c4-423d-8cfc-06b8b670bca3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/6.2 Add Unplan - MR membuat Unplan dengan tipe customer Non Doctor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eac5222-9ef4-41dc-acdb-e112eb5e8489</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/6.3 Add Unplan - MR membuat Unplan dengan tipe customer Outlet</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9ed6127-3acc-4e9d-abcf-6a272155a09b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/7.1 Edit Unplan - MR melakukan edit Unplan status New request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65d8ae3b-1c47-4d00-8a2b-f6a25a6b5043</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/7.2 Edit Unplan - MR membuat unplan sampai dengan tanda tangan lalu melakukan reset pada tanda tangan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8f8b88b-4ef3-41ae-b9f7-a8be09edf30e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/8.1 Draft Unplan - MR input tanda tangan yang benar lalu save as draft</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b7a1e883-6b89-4a35-94a4-49f99e8d1f67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/9.1 Delete Unplan - MR melakukan delete unplan status New Request (before synch)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6001425e-346e-456d-b815-3ec5b0abfe0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/9.2 Delete Unplan - MR melakukan delete unplan status Draft (before synch)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cc50a88-1659-49ae-b18b-b974d56538c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/10.2 PIMDA - Pimda reject call plan new request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d86971be-9cc9-45c3-97e1-2592e61f6651</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/10.1 PIMDA - Pimda approve data call plan yang sudah dibuat MR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29a593a9-29c6-4fbd-bbeb-d5e16814f8c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/11.1 Add Realisasi - MR Membuat realisasi dari menu To Do List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>225ed2fe-1cec-4d47-9570-d8f174f24110</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/11.2 Edit Realisasi - MR Melakukan edit realisasi dengan mengubah salah satu field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2944d7a6-0990-4983-9efe-803ecf6cd63b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/12.2 PIMDA - Pimda Reject data realisasi yang sudah dibuat MR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55533521-39bc-400f-ad03-9096e678203e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/12.1 PIMDA - Pimda Approve data realisasi yang sudah dibuat MR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0443a0be-27fd-43d0-b9f4-324d421e3b9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/12.4 PIMDA - Pimda reject unplan new request</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>300734df-3f2b-4f52-af56-c8eb98e0e024</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/12.3 PIMDA - Pimda Approve data Unplan yang sudah dibuat MR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f5e523a-c3f3-4072-b070-11d39e725965</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/13.1 GPS - Cek maps di unplan online gps mati</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1499b06c-4a8f-4980-aa0f-4519a9f06b62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/13.2 GPS - Cek maps di unplan online gps nyala</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8db631e-d2d6-4c60-b360-2a74e45e9d8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/13.3 GPS - Cek maps di realisasi plan online gps mati</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>afe66482-d043-4526-8e41-c79c3ce5ab99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/13.4 GPS - Cek maps di realisasi plan online gps nyala</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>620eeeb1-80e9-4071-87aa-9c387f6d1c62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/14.1 GPS (offline) - Cek maps di unplan offline gps mati</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>728e44de-c464-4475-908d-d468ed3aa4b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/14.2 GPS (offline) - Cek maps di unplan offline gps nyala</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>806629b9-4aff-49ed-97de-4376ec5a435d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/14.3 GPS (offline) - Cek maps di realisasi plan offline gps mati</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be36f74-674b-4934-bbfe-84a8b0bf8ca8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Call Plan Management/14.4 GPS (offline) - Cek maps di realisasi plan offline gps nyala</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
