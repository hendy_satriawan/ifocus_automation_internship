<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>onBoarding</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>cd3defec-5174-4193-a1e7-ea5e13f8642b</testSuiteGuid>
   <testCaseLink>
      <guid>5bd79355-c914-412a-a82c-1fb1040c3f80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.2 Klik button Next pada onboarding page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7960350f-4fb5-44ae-b73f-0adeedf0b597</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.1 Klik button Skip pada onboarding page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1409e7f8-d859-4dd3-8311-919922d2feed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.3 Swipe pada onboarding page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
