<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fee49465-6d42-4246-823d-1d895a9ac787</testSuiteGuid>
   <testCaseLink>
      <guid>e91d5631-6110-4472-9194-ba88d0fcb347</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Trash/loginDataBinding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>b460256d-c6ef-401f-9e1c-759c38676a72</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/login/loginOnline</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>b460256d-c6ef-401f-9e1c-759c38676a72</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>28bf42e5-e221-41b2-aa85-f5b77144fe62</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>b460256d-c6ef-401f-9e1c-759c38676a72</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>3197d400-ee5c-47fe-9ec7-4d2e29f1850d</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
