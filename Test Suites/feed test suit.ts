<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>feed test suit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>ccb244a0-b499-4a5a-a911-a974ca4385b3</testSuiteGuid>
   <testCaseLink>
      <guid>db7e6642-ed3f-4fe1-9933-0f80075b5e5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.1 Klik button Skip pada onboarding page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29aad415-a51a-4b27-b4e0-9b616b7a65cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.2 Klik button Next pada onboarding page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8a8e184-6a6f-4863-943e-016ee99771fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Onboarding/1.3 Swipe pada onboarding page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>725679d9-c2eb-42f8-b1e5-558830eec64c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Login/2.1-2.5 Login Online (Menguji Inputan Login)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6447ce2-1559-45c7-8623-119094863b50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Login/3.1-3.4 Login Offline (Menguji Inputan Login)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bac96991-7a28-44b5-b96e-b77bb167164e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/4.1-4.7 Video (View, Check, Share D2D-Team, Bookmark, Unbookmark, Rotete)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>483fcea8-8d69-4cf9-883b-47027c69cd54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/4.8-4.13 Literarur (View, Check, Share D2D-Team, Bookmark, Unbookmark)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c471824-6254-4663-958c-d042c7f79439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/4.14-4.20 Brosur (View, Check, Share D2D-Team, Batal share, Bookmark, Unbookmark)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>700d7272-7690-484a-9746-9080c6ff61e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.1 Search feed lalu view hasil searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2d2699d-9982-4cdc-bcc3-108ea4af6d58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.2 Search Feed klik icon X setelah input keywords pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0ceb96f-ce4f-4df5-bd6c-33dd71009b99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.3 Search Feed klik icon Nav Back pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f6aae53-4361-4fab-aea0-ea16d626af60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.4 Search Feed lalu bookmark hasil searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>787c7f69-3cac-4355-9b4f-51ca20dd04ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.5 search feed lalu unbookmark hasil searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd93bffe-5cb4-4358-85ca-7f4ef7c35e0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/5.6 search feed lalu share hasil searching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15125065-c431-4e73-b0f6-1f1062ca6e0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/6.1 Filter - klik icon X setelah input keywords pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae404427-dd22-4b96-a5ac-3e852e9048dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/6.2 Filter - klik icon Nav Back pada search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7165012e-148c-43cc-a9aa-8fdf38f1ff62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/6.3 Filter - pilih batal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbd5e6c5-e201-49ac-9c4a-02a563917633</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/6.4 Filter - terapkan tanpa pilih produk yang difilter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1d6d2179-6aab-4329-b205-a23e5cc8881a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/6.5 Filter list feed lalu terapkan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c22186c-f536-4901-adc8-ab160ede68e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Feed/Pengujian Feed/7.1-7.20 Feed Fetured Product (Brosur, Knowledge, Literatur, Video, Deskripsi)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9dbbfb98-3f72-4d8c-977d-cf41cad68ecc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/productKnowladge/tab/tabBrosurTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96e4f589-96e1-4f37-87e3-2002f17bef43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/productKnowladge/tab/tabKnowladgeTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>625c5ade-4638-494c-8e59-3b7f7350ee4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/productKnowladge/tab/tabVideoTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f76ac3c3-a1ca-4781-b215-3cc2dda38280</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/productKnowladge/tab/tabLiteraturTestCase</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>187fd936-f01c-4363-bab2-f09a0ab9ed90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Explore/productKnowladge/tab/tabDeskripsiTestCase</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
