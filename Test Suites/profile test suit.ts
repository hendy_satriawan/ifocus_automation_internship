<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>profile test suit</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a73e8937-f8d5-435a-b4ec-9c4b4577e0f0</testSuiteGuid>
   <testCaseLink>
      <guid>50deb032-bd96-4459-b3a8-a8f922db60d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.1 Profile - Melihat detail profile user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2996795-6632-4c3e-8c39-25436d4a4b62</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.2 Profile - Melihat list bookmark (kondisi online)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85e0b8fd-4a65-44d0-aedf-3a470af15bd9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.3 Profile - Melihat detail bookmark - video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8ed1a74-a9e5-4e63-acf1-4daa770df71c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.4 Profile - Melihat detail bookmark - pdf</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04ae60d2-7d71-4a2d-86dc-acfe64a2d823</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.5 Profile - Share hasil bookmark video ke dokter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c6dffbb-fd3a-4393-814d-fe5330a405a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.6 Profile - Share hasil bookmark video ke Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7867eb9e-91e0-41ed-9f30-614f50576695</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.7 Profile - Share hasil bookmark pdf ke D2D</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9865062e-9664-4749-b173-bfcb3de9315d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.8 Profile - Share hasil bookmark pdf ke Team</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85e73914-69b2-4e07-9f98-8da31a47e90b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.9 Profile - batal share dengan pilih CLOSE</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73718f5c-b319-48b7-bb90-ed9047105f64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.10 Profile - Unbookmark video pada menu profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca074874-083a-40b6-9170-cd42503c331c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.11 Profile - Unbookmark pdf pada menu profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11716646-5d7c-4b4c-bf32-e6f140c7b5fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.12 Profile - Melihat list download (kondisi online)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aba22e11-9fdc-41b9-8b1b-453678fbb0a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.13 Profile - Melihat detail download pdf</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9d2adbfc-a35c-4bf5-8628-b612e35c534f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.14 Profile - Melihat detail download video</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dea17ffc-4392-4c0c-b96e-b4eb49bbd816</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.15 Profile - Clear data call plan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a34297fa-44a9-4de4-936c-c94c5c9275ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.16 Profile - Melakukan emergency sync kondisi online</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a5b7fd8-bfcf-4e93-9ce4-e3e63b681392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.17 Profile - User melakukan logout kondisi online</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5509bdd-ab93-4451-b203-9039ba5c8658</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.18 Profile - Melihat list bookmark (kondisi offline)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>648a00a2-1c76-4ce4-b71d-19e3a715644a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.19 Profile - Melihat list download (kondisi offline)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dedd7059-3fec-4ff5-bda3-43c45a44cac7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.20 Profile - Melakukan emergency sync kondisi offline</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cdbffa86-b04c-4558-b8cc-e2a0ad8b815a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/1.21 Profile - User melakukan logout kondisi offline</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
