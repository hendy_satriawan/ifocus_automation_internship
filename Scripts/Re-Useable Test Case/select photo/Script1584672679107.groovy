import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/Unplan/btnTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

if (GlobalVariable.Username == 'JK3VIC0103' || GlobalVariable.Username == 'PWKVIC0103') {
    Mobile.waitForElementPresent(findTestObject('select Photo/TextView - Choose from Library'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('select Photo/TextView - Choose from Library'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('select Photo/TextView - Galeri'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('select Photo/TextView - Galeri'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('select Photo/ImageView (Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('select Photo/ImageView (Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('select Photo/TextView - Take Photo'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('select Photo/TextView - Take Photo'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(4)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnTapPicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnTapPicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(4)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnConfirmTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnConfirmTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)
} else if (GlobalVariable.Username == 'JK3MAX0406') {
    Mobile.delay(4)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnTapPicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnTapPicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(4)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnConfirmTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnConfirmTakePicture'), 0, FailureHandling.STOP_ON_FAILURE)
}

