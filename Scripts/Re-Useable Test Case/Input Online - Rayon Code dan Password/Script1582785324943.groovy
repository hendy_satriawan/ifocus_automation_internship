import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

Mobile.setText(findTestObject('Feed/login/RayonCode'), username, 5)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), password, 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//Jika gagal login makan dialogbox akan keluar dan tap button OK
if (Mobile.verifyElementExist(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.OPTIONAL) == true) {
    Mobile.tap(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Clear Text - Rayon Code dan Password'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.setText(findTestObject('Feed/login/RayonCode'), username2, 5)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), password2, 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//Jika gagal login makan dialogbox akan keluar dan tap button OK
if (Mobile.verifyElementExist(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.OPTIONAL) == true) {
    Mobile.tap(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Clear Text - Rayon Code dan Password'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.setText(findTestObject('Feed/login/RayonCode'), username3, 5)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), password3, 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//Jika gagal login makan dialogbox akan keluar dan tap button OK
if (Mobile.verifyElementExist(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.OPTIONAL) == true) {
    Mobile.tap(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Clear Text - Rayon Code dan Password'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.setText(findTestObject('Feed/login/RayonCode'), username4, 5)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), password4, 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

//Jika gagal login makan dialogbox akan keluar dan tap button OK
if (Mobile.verifyElementExist(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.OPTIONAL) == true) {
    Mobile.tap(findTestObject('Feed/login/trash/btnAlert_OK'), 0, FailureHandling.CONTINUE_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Clear Text - Rayon Code dan Password'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.setText(findTestObject('Feed/login/RayonCode'), username5, 5)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), password5, 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

