import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 0)

Mobile.tap(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/cekDetail/view'), 0)

not_run: Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/view'), 0)

not_run: if (Mobile.verifyElementExist(findTestObject('FeaturedProduct/knowladge/downloadKnowladge'), 0, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('FeaturedProduct/knowladge/downloadKnowladge'), 0)
} else if (Mobile.verifyElementExist(findTestObject('Feed/FeaturedProduct/downloadBrosur'), 0, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('Feed/FeaturedProduct/downloadBrosur'), 0)
} else {
    Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/Download'), 0)
}

not_run: Mobile.pressBack()

not_run: closed = Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/cekDetail/bookMark'), 0)

not_run: if (!(closed)) {
    Mobile.startApplication('app\\ifocus-dev.apk', false)

    Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/android.widget.TextView0 -  Aminefron'), 0)

    Mobile.tap(findTestObject('Feed/FeaturedProduct/android.widget.TextView0 -  Aminefron'), 0)

    Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 
        0)

    Mobile.tap(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 0)
}

not_run: Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/bookMark'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/cekDetail/bookMark'), 0)

not_run: Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/bookMark'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/cekDetail/share'), 0)

not_run: Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/share'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('Feed/Share_BookMark/android.widget.TextView0 - CLOSE'), 0)

not_run: Mobile.tap(findTestObject('Feed/Share_BookMark/android.widget.TextView0 - CLOSE'), 0)

