import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

Mobile.startApplication(GlobalVariable.appName, false)

ada = Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 10)

if (ada == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VIC0103', ('password') : 'a'], FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 3)
}

for (def index : (0..3)) {
    Mobile.swipe(500, 2000, 500, 200)
}

Mobile.scrollToText('Brosur Inlacin', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feed/ViewListPK/android.widget.TextView0 - Brosur Vometa Syr'), 0)

Mobile.pressBack()

Mobile.tap(findTestObject('Feed/Share_BookMark/Button Share'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feed/Share_BookMark/android.widget.TextView0 - Dokter'), 0)

Mobile.tap(findTestObject('Feed/Share_BookMark/BatalShare'), 0)

Mobile.tap(findTestObject('Feed/Share_BookMark/Button Share'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feed/Share_BookMark/android.widget.TextView0 - Team'), 0)

Mobile.tap(findTestObject('Feed/Share_BookMark/BatalShare'), 0)

Mobile.tap(findTestObject('Feed/Share_BookMark/Button Share'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feed/Share_BookMark/android.widget.TextView0 - CLOSE'), 0)

Mobile.tap(findTestObject('Feed/Share_BookMark/Button Bookmark'), 0)

Mobile.delay(2, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Feed/Share_BookMark/Button Bookmark'), 0)

Mobile.closeApplication()

