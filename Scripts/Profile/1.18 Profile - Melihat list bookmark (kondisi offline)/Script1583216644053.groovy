import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('Feed/login/offlineCheck (2)'), 0)

Mobile.checkElement(findTestObject('Feed/login/offlineCheck (2)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Feed/login/Password'), 0)

Mobile.setText(findTestObject('Feed/login/Password'), 'a', 5)

Mobile.tap(findTestObject('Feed/login/btnContinue'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('Profile/Profile'), 0)

Mobile.tap(findTestObject('Profile/Profile'), 0)

Mobile.waitForElementPresent(findTestObject('Profile/bookmarked/btnBookmarked'), 0)

Mobile.tap(findTestObject('Profile/bookmarked/btnBookmarked'), 0)

Mobile.tap(findTestObject('Profile/bookmarked/offlineMode'), 3, FailureHandling.OPTIONAL)

Mobile.verifyElementNotExist(findTestObject('Profile/bookmarked/viewGroup_content1'), 3)

Mobile.waitForElementPresent(findTestObject('Profile/bookmarked/navBack'), 0)

Mobile.tap(findTestObject('Profile/bookmarked/navBack'), 0)

