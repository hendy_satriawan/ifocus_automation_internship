import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

ada = Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 10)

if (ada == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VIC0103', ('password') : 'a'], FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 3)
}

Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/android.widget.TextView0 -  Aminefron'), 0)

Mobile.tap(findTestObject('Feed/FeaturedProduct/android.widget.TextView0 -  Aminefron'), 0)

Mobile.waitForElementPresent(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 0)

Mobile.tap(findTestObject('Feed/FeaturedProduct/detailProduct/android.widget.TextView0 - VIDEOS'), 0)

adaIsi = Mobile.verifyElementExist(findTestObject('Feed/FeaturedProduct/cekDetail/view'), 3, FailureHandling.OPTIONAL)

if (adaIsi == true) {
    Mobile.callTestCase(findTestCase('Explore/productKnowladge/trash/tabVideo/downloadVideo'), [:], FailureHandling.STOP_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Explore/productKnowladge/trash/tabVideo/share'), [:], FailureHandling.STOP_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Explore/productKnowladge/trash/tabVideo/bookMark_unbookMark'), [:], FailureHandling.STOP_ON_FAILURE)
} else {
    Mobile.comment('Belum ada item')
}

