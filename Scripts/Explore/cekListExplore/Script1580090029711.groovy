import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

Mobile.startApplication('app\\ifocus-dev.apk', false)

Mobile.waitForElementPresent(findTestObject('Explore/menuExplore/Explore'), 0)

Mobile.tap(findTestObject('Explore/menuExplore/Explore'), 0)

Mobile.verifyElementExist(findTestObject('Explore/menuExplore/productKnowladge'), 0)

Mobile.verifyElementExist(findTestObject('Explore/menuExplore/journal'), 0)

Mobile.verifyElementExist(findTestObject('Explore/menuExplore/videos'), 0)

Mobile.verifyElementExist(findTestObject('Explore/menuExplore/event'), 0)

Mobile.callTestCase(findTestCase('Explore/1.1 Eksplore - buka menu explore dan cek list menu yang muncul'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.pressBack()

Mobile.callTestCase(findTestCase('Explore/2.1 Video - View list video pada tab terbaru'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/Journal/journalTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

