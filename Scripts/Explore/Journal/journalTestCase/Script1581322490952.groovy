import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

Mobile.waitForElementPresent(findTestObject('Explore/menuExplore/Explore'), 0)

Mobile.tap(findTestObject('Explore/menuExplore/Explore'), 0)

Mobile.verifyElementExist(findTestObject('Explore/menuExplore/journal'), 0)

Mobile.tap(findTestObject('Explore/menuExplore/journal'), 0)

Mobile.callTestCase(findTestCase('Explore/1.2 Journal - View list menu journal pada tab terbaru'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.8 Journal - View detail journal pada tab terbaru'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.9 Journal - klik icon Nav Back untuk kembali ke list journal'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.10 Journal - klik icon X setelah input keywords pada search'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('null'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.11 Journal - Pilih Batal'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.12 Journal - pilih Terapkan tanpa pilih produk yang difilter'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Explore/1.13 Journal - Filter Journal'), [:], FailureHandling.STOP_ON_FAILURE)

