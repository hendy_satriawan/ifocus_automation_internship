import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

adaIsi = Mobile.verifyElementExist(findTestObject('Feed/FeaturedProduct/cekDetail/view'), 3, FailureHandling.OPTIONAL)

if (adaIsi == true) {
    Mobile.tap(findTestObject('Feed/FeaturedProduct/cekDetail/view'), 0)

    downloaded = Mobile.verifyElementNotVisible(findTestObject('Explore/productKnowladge/downloadPK'), 3, FailureHandling.OPTIONAL)

    if (downloaded == false) {
        Mobile.tap(findTestObject('Explore/productKnowladge/downloadPK'), 5)
    } else {
        Mobile.comment('item sudah terdownload')
    }
    
    Mobile.pressBack()
} else {
    Mobile.comment('Belum ada item')
}

