import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.startApplication(GlobalVariable.appName, false)

not_run: ada = Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 10)

not_run: if (ada == false) {
    not_run: Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VIC0103', ('password') : 'a'], 
        FailureHandling.STOP_ON_FAILURE)

    not_run: Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 3)
}

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/tabUnplan'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/Unplan/tabUnplan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('New Request', FailureHandling.OPTIONAL)

isNewReq = Mobile.verifyElementExist(findTestObject('CallPlan/editCallPlan/getStatusNewReq'), 0, FailureHandling.OPTIONAL)

if (isNewReq == true) {
    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/delete/DeleteX'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/delete/DeleteX'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/deleteCallPlan/btnDeleteYES'), 0)

    Mobile.tap(findTestObject('CallPlan/deleteCallPlan/btnDeleteYES'), 0)
} else {
    Mobile.comment(isNewReqSynced + ': Tidak ada New Request Call Plan')
}

