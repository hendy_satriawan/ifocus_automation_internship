import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.startApplication(GlobalVariable.appName, false)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/inputTanggal1'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/tanggal/inputTanggal1'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/tabRealisasi(xpath)'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/tabRealisasi(xpath)'), 0)

ada = Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/itemCallPlan'), 0)

if (ada == true) {
    Mobile.tap(findTestObject('CallPlan/editCallPlan/itemCallPlan'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0)

    if (GlobalVariable.Username == 'JK3MAX0406') {
        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemProduct-Biobran(Text)'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/itemProduct-Biobran(Text)'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/brosurBiobran'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/brosurBiobran'), 0, FailureHandling.STOP_ON_FAILURE)
    } else if (GlobalVariable.Username == 'JK3VIC0103') {
        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/brosurAminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/brosurAminefron'), 0, FailureHandling.STOP_ON_FAILURE)
    }
    
    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnFloat'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnFloat'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext2'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext2'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext3'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext3'), 0)

    not_run: Mobile.clearText(findTestObject('CallPlan/Unplan/inputTextPorductNOTE'), 0)

    not_run: Mobile.setText(findTestObject('CallPlan/Unplan/inputTextPorductNOTE'), 'regressionTesting EDITUnplan', 0)

    Mobile.scrollToText('SAVE', FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

    not_run: Mobile.pressBack()

    Mobile.pressBack()

    Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)
}

