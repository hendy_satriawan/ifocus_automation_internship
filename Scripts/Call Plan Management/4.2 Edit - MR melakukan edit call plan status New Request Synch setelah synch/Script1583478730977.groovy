import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.startApplication(GlobalVariable.appName, false)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

'aktifkan jika run seluruh test suit\r\n'
not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

Mobile.scrollToText('New Request Synced', FailureHandling.STOP_ON_FAILURE)

isNewReqSynced = Mobile.verifyElementExist(findTestObject('CallPlan/editCallPlan/status-New Request Synced'), 0, FailureHandling.OPTIONAL)

if (isNewReqSynced == true) {
    Mobile.tap(findTestObject('CallPlan/editCallPlan/status-New Request Synced'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerStatus'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/spinnerStatus'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/selectStatus-COVERAGE'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/selectStatus-COVERAGE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerChanel'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/spinnerChanel'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0)

    Mobile.tap(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/checkBox_activity-LITERATUR'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/checkBox_activity-LITERATUR'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSaveEdit'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSaveEdit'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnSuccess-YES'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/btnSuccess-YES'), 0)
} else {
    Mobile.comment(isNewReqSynced + ': Tidak ada New Request Call Plan')
}

