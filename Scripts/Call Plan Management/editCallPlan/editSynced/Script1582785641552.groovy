import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

isNewReqSynced = Mobile.verifyElementExist(findTestObject('CallPlan/editCallPlan/status-New Request Synced'), 0, FailureHandling.OPTIONAL)

if (isNewReqSynced == true) {
    Mobile.tap(findTestObject('CallPlan/editCallPlan/itemCallPlan'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerStatus'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/spinnerStatus'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/selectStatus-COVERAGE'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/selectStatus-COVERAGE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerChanel'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/spinnerChanel'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/selectChanel-RS SWASTA'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/selectChanel-RS SWASTA'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/checkBox_activity-LITERATUR'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/checkBox_activity-LITERATUR'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSaveEdit'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSaveEdit'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnSuccess-YES'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/btnSuccess-YES'), 0)
} else {
    Mobile.comment(isNewReqSynced + ': Tidak ada New Request Call Plan')
}

Mobile.pressBack()

