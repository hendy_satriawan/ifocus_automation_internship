import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

isLogin = Mobile.verifyElementExist(findTestObject('Feed/login/RayonCode'), 10, FailureHandling.OPTIONAL)

if (isLogin == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/LogOut'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VIC0103', ('password') : 'a'], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/1.1 Synch - MR melakukan proses synchronization agar data sama antara db local dan server'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/callOverview'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/masterList/masterListTestCase'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/editCallPlan/editNotSyncYet'), [:], FailureHandling.OPTIONAL)

Mobile.callTestCase(findTestCase('Call Plan Management/1.1 Synch - MR melakukan proses synchronization agar data sama antara db local dan server'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/editCallPlan/editSynced'), [:], FailureHandling.OPTIONAL)

Mobile.callTestCase(findTestCase('Call Plan Management/delete/deleteSynced'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal1'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/tabUnplan'), 0)

Mobile.tap(findTestObject('CallPlan/Unplan/tabUnplan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/addUnplan/addUnplan'), [('customer') : 'Doctor'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/addUnplan/addUnplan'), [('customer') : 'Non-Doctor'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/addUnplan/addUnplan'), [('customer') : 'draft'], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/editUnplan/editUnplan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/delete/hapusXCallPlan'), [:], FailureHandling.STOP_ON_FAILURE)

