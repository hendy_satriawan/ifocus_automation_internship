import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal3'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal3'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/delete/hapusXCallPlan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.waitForElementPresent(findTestObject('CallPlan/addThrouMasterlist/menu_MasterList'), 0)

Mobile.tap(findTestObject('CallPlan/addThrouMasterlist/menu_MasterList'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/addThrouMasterlist/editTextSearch'), 0)

Mobile.setText(findTestObject('CallPlan/addThrouMasterlist/editTextSearch'), 'Hendrikson Muhamad', 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/addThrouMasterlist/itemMasterList'), 0)

Mobile.tap(findTestObject('CallPlan/addThrouMasterlist/itemMasterList'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/masterList/addPlan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.clearText(findTestObject('CallPlan/addThrouMasterlist/editTextSearch'), 0)

Mobile.setText(findTestObject('CallPlan/addThrouMasterlist/editTextSearch'), 'Wiwin Taqwimah', 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/addThrouMasterlist/itemMasterList'), 0)

Mobile.tap(findTestObject('CallPlan/addThrouMasterlist/itemMasterList'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/masterList/addPlan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.pressBack()

