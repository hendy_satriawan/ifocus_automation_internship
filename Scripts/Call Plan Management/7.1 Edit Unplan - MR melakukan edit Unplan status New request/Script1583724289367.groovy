import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

ada = Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 20)

if (ada == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VIC0103', ('password') : 'a'], FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('Feed/ViewListPK/feed'), 3)
}

Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/tabUnplan'), 0)

Mobile.tap(findTestObject('CallPlan/Unplan/tabUnplan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('New Request', FailureHandling.OPTIONAL)

adaIsi = Mobile.verifyElementExist(findTestObject('CallPlan/editCallPlan/getStatusNewReq'), 5, FailureHandling.OPTIONAL)

if (adaIsi == true) {
    Mobile.tap(findTestObject('CallPlan/editCallPlan/getStatusNewReq'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNEXT (1)'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNEXT (1)'), 0)

    not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0)

    not_run: Mobile.tap(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0)

    not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0)

    not_run: Mobile.tap(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0)

    not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/brosurAminefron'), 0)

    not_run: Mobile.tap(findTestObject('CallPlan/Unplan/brosurAminefron'), 0)

    not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnFloat'), 0)

    not_run: Mobile.tap(findTestObject('CallPlan/Unplan/btnFloat'), 0)

    not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/resetTTD'), 0)

    not_run: Mobile.tap(findTestObject('CallPlan/Unplan/resetTTD'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext2'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext2'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext3'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext3'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/edit/spinnerResult'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/edit/spinnerResult'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/edit/itemResult - COMMITED'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/edit/itemResult - COMMITED'), 0)

    Mobile.scrollToText('SAVE', FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)
} else {
    Mobile.comment('tidak ada data NEW REQUEST')
}

