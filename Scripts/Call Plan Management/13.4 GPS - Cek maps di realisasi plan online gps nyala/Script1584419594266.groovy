import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

not_run: Mobile.startApplication(GlobalVariable.appName, false)

not_run: isLogin = Mobile.verifyElementExist(findTestObject('Feed/login/RayonCode'), 3, FailureHandling.OPTIONAL)

not_run: if (isLogin == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/LogOut'), [:], FailureHandling.STOP_ON_FAILURE)
}

not_run: Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : GlobalVariable.Username, ('password') : GlobalVariable.Password], 
    FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/GPS/prevMount (Xpath)'), 0)

'diubah sesuai dengan tgl realisasi yg ada'
not_run: Mobile.tap(findTestObject('CallPlan/GPS/prevMount (Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

'diubah sesuai dengan tgl realisasi yg ada'
not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/GPS/tgl - 25'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/GPS/tgl - 25'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/tabRealisasi(xpath)'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/PIMDA/tabRealisasi(xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/itemCallPlan'), 0)

Mobile.tap(findTestObject('CallPlan/editCallPlan/itemCallPlan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext2'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/Unplan/btnNext2'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext3'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/Unplan/btnNext3'), 0, FailureHandling.STOP_ON_FAILURE)

not_run: Mobile.delay(4, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/GPS/mapView2 (Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/GPS/mapView2 (Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/GPS/Refresh Maps (Text)'), 0)

Mobile.tap(findTestObject('CallPlan/GPS/Refresh Maps (Text)'), 0)

Mobile.scrollToText('SAVE', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

