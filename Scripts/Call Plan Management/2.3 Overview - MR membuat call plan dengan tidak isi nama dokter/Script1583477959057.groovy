import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnAdd'), 0)

Mobile.tap(findTestObject('CallPlan/overView/btnAdd'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/radioCustomer_Doctor'), 0)

Mobile.tap(findTestObject('CallPlan/overView/radioCustomer_Doctor'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerStatus'), 0)

Mobile.tap(findTestObject('CallPlan/overView/spinnerStatus'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/status-DEFECTOR'), 0)

Mobile.tap(findTestObject('CallPlan/overView/status-DEFECTOR'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/spinnerChanel'), 0)

Mobile.tap(findTestObject('CallPlan/overView/spinnerChanel'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0)

Mobile.tap(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/checkBox_Activity-ENTERTAINTMENT'), 0)

Mobile.tap(findTestObject('CallPlan/overView/checkBox_Activity-ENTERTAINTMENT'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/inputDate'), 0)

Mobile.tap(findTestObject('CallPlan/overView/inputDate'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/inputTanggal1'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/inputTanggal1'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnOkeDate'), 0)

Mobile.tap(findTestObject('CallPlan/overView/btnOkeDate'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnSave_CallPlan'), 0)

Mobile.tap(findTestObject('CallPlan/overView/btnSave_CallPlan'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/dialogBoxError'), 0, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('CallPlan/overView/btnOK_dialogBox'), 0)

Mobile.pressBack()

