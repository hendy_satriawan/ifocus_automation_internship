import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/tabUnplan'), 0)

Mobile.tap(findTestObject('CallPlan/Unplan/tabUnplan'), 0, FailureHandling.STOP_ON_FAILURE)

ada = Mobile.waitForElementPresent(findTestObject('CallPlan/nama dokter/TextView - WIWIN TAQWIMAH - Copy'), 5)

if (ada == false) {
    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/btnAdd'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/btnAdd'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/radioNonDoctor'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/radioNonDoctor'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/inputTextCustemer'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('CallPlan/Unplan/inputTextCustemer'), GlobalVariable.UnplanNonDoctor, 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/selectCustomer'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/selectCustomer'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/spinnerStatus'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/spinnerStatus'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemSpinnerStatus-RETENSI A'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/itemSpinnerStatus-RETENSI A'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/spinnerChanel'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/spinnerChanel'), 0, FailureHandling.STOP_ON_FAILURE)

    if (GlobalVariable.Username == 'JK3MAX0406') {
        Mobile.waitForElementPresent(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/plan/Chanelitem1(Xpath)'), 0, FailureHandling.STOP_ON_FAILURE)
    } else if ((GlobalVariable.Username == 'JK3VIC0103') || (GlobalVariable.Username == 'PWKVIC0103')) {
        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemSpinnerChanel-HOSPITAL'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/itemSpinnerChanel-HOSPITAL'), 0, FailureHandling.STOP_ON_FAILURE)
    }
    
    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNEXT'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNEXT'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/iconMenuProduct'), 0, FailureHandling.STOP_ON_FAILURE)

    if (GlobalVariable.Username == 'JK3MAX0406') {
        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemProduct-Biobran(Text)'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/itemProduct-Biobran(Text)'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/brosurBiobran'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/brosurBiobran'), 0, FailureHandling.STOP_ON_FAILURE)
    } else if ((GlobalVariable.Username == 'PWKVIC0103') || (GlobalVariable.null == 'JK3VIC0103')) {
        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/itemProduct-Aminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/brosurAminefron'), 0, FailureHandling.STOP_ON_FAILURE)

        Mobile.tap(findTestObject('CallPlan/Unplan/brosurAminefron'), 0, FailureHandling.STOP_ON_FAILURE)
    }
    
    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnFloat'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnFloat'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(2)

    Mobile.swipe(200, 1000, 300, 1600)

    Mobile.swipe(300, 1600, 400, 1000)

    Mobile.swipe(400, 1000, 500, 1600)

    Mobile.swipe(500, 1600, 600, 1000)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/saveTTD'), 0)

    Mobile.tap(findTestObject('CallPlan/Unplan/saveTTD'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/Toast-Signature saved successfully'), 2)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext2'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext2'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.callTestCase(findTestCase('Re-Useable Test Case/select photo'), [:], FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnNext3'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnNext3'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/inputTextPorductNOTE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.setText(findTestObject('CallPlan/Unplan/inputTextPorductNOTE'), 'resgressionTesting Product NOTE', 0)

    Mobile.scrollToText('SAVE', FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/scrollText-SAVE'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.waitForElementPresent(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.tap(findTestObject('CallPlan/Unplan/btnSuccess'), 0, FailureHandling.STOP_ON_FAILURE)

    Mobile.delay(3, FailureHandling.STOP_ON_FAILURE)

    Mobile.pressBack()

    Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

    Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)
}

not_run: Mobile.closeApplication()

