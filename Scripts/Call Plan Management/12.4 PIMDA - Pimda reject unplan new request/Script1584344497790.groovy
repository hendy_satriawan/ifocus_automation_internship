import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

isLogin = Mobile.verifyElementExist(findTestObject('Feed/login/RayonCode'), 20, FailureHandling.OPTIONAL)

if (isLogin == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/LogOut'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : GlobalVariable.Username2, ('password') : GlobalVariable.Password2], 
    FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

noSynchronized = Mobile.waitForElementPresent(findTestObject('CallPlan/Synchronize status/TextView - last update  -'), 5, 
    FailureHandling.CONTINUE_ON_FAILURE)

if (noSynchronized == true) {
    Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

    Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

    Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)
}

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/menu_Approval'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/menu_Approval'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/searchBox'), 0)

Mobile.setText(findTestObject('CallPlan/PIMDA/searchBox'), GlobalVariable.Bawahan, 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/itemEmployee'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/itemEmployee'), 0)

Mobile.waitForElementPresent(findTestObject('todoList/Unplan-Aproval(Xpath)'), 0)

Mobile.tap(findTestObject('todoList/Unplan-Aproval(Xpath)'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/itemDokter'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/itemDokter'), 0)

Mobile.waitForElementPresent(findTestObject('todoList/itemApprovalList (Xpath)'), 0)

Mobile.tap(findTestObject('todoList/itemApprovalList (Xpath)'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/tabDetailRealisasi - REALIZATION'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/tabDetailRealisasi - REALIZATION'), 0)

Mobile.scrollToText('APPROVE', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/spinnerEffective (1)'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/spinnerEffective (1)'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/item Effective - Effective'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/item Effective - Effective'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/detailRealisasi - REJECT'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/detailRealisasi - REJECT'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/inputNOTE'), 0)

Mobile.setText(findTestObject('CallPlan/PIMDA/inputNOTE'), 'Testing Regression', 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnRejectNOTE - SUBMIT'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/btnRejectNOTE - SUBMIT'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/approval_reject-YES'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/approval_reject-YES'), 0)

not_run: Mobile.pressBack()

not_run: Mobile.pressBack()

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/PIMDA/btnSYNCHRONIZE_NOW'), 0)

not_run: Mobile.waitForElementPresent(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

not_run: Mobile.tap(findTestObject('CallPlan/editCallPlan/btnSuccessYes'), 0)

Mobile.closeApplication()

