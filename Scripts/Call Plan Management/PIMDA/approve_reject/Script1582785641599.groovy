import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.appName, false)

isLogin = Mobile.verifyElementExist(findTestObject('Feed/login/RayonCode'), 0, FailureHandling.OPTIONAL)

if (isLogin == false) {
    Mobile.callTestCase(findTestCase('Re-Useable Test Case/LogOut'), [:], FailureHandling.STOP_ON_FAILURE)
}

Mobile.callTestCase(findTestCase('Re-Useable Test Case/Login'), [('username') : 'JK3VICS01', ('password') : 'a'], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/CallPan'), 0)

Mobile.tap(findTestObject('CallPlan/CallPan'), 0)

not_run: Mobile.callTestCase(findTestCase('Call Plan Management/1.1 Synch - MR melakukan proses synchronization agar data sama antara db local dan server'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/menu_Approval'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/menu_Approval'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/searchBox'), 0)

Mobile.setText(findTestObject('CallPlan/PIMDA/searchBox'), 'Deni Nurpratama', 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/itemEmployee'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/itemEmployee'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/PIMDA/approve'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/PIMDA/reject'), [:], FailureHandling.STOP_ON_FAILURE)

