import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/itemDokter'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/itemDokter'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/checkBox_NO'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/checkBox_NO'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/inputNOTE'), 0)

Mobile.setText(findTestObject('CallPlan/PIMDA/inputNOTE'), 'Testing Regression', 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSaveNOTE'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/btnSaveNOTE'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/btnSAVE'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/btnSAVE'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/PIMDA/approval_reject-YES'), 0)

Mobile.tap(findTestObject('CallPlan/PIMDA/approval_reject-YES'), 0)

