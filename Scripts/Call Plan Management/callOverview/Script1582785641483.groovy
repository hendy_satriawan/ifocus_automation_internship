import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.waitForElementPresent(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.tap(findTestObject('CallPlan/overView/menu_CallOverView'), 0)

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal1'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/delete/hapusXCallPlan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/addPlan'), [('customer') : 'Doctor'], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/addPlan'), [('customer') : 'Non Doctor'], FailureHandling.CONTINUE_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('CallPlan/tanggal/tabTanggal2'), 0)

Mobile.tap(findTestObject('CallPlan/tanggal/tabTanggal2'), 0)

Mobile.callTestCase(findTestCase('Call Plan Management/delete/hapusXCallPlan'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/2.8 Overview - MR membuat call plan dengan lebih dari 1 tanggal'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/tanpaNamaDokter'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/tanpaStatus'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/tanpaChanel'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/tanpaActivities'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.callTestCase(findTestCase('Call Plan Management/sub/tanpaDate'), [:], FailureHandling.STOP_ON_FAILURE)

