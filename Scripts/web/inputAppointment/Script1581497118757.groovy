import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementPresent(findTestObject('webTest/Page_CURA Healthcare Service/selectFacility'), 0)

WebUI.selectOptionByValue(findTestObject('webTest/Page_CURA Healthcare Service/selectFacility'), 'Hongkong CURA Healthcare Center', 
    true)

WebUI.selectOptionByValue(findTestObject('webTest/Page_CURA Healthcare Service/selectFacility'), 'Tokyo CURA Healthcare Center', 
    true)

WebUI.selectOptionByValue(findTestObject('webTest/Page_CURA Healthcare Service/selectFacility'), 'Seoul CURA Healthcare Center', 
    true)

WebUI.waitForElementPresent(findTestObject('webTest/Page_CURA Healthcare Service/checkBox_Readmission'), 0)

WebUI.check(findTestObject('webTest/Page_CURA Healthcare Service/checkBox_Readmission'))

WebUI.waitForElementPresent(findTestObject('webTest/Page_CURA Healthcare Service/radio_Medicaid_programs'), 0)

WebUI.check(findTestObject('webTest/Page_CURA Healthcare Service/radio_Medicaid_programs'))

WebUI.waitForElementPresent(findTestObject('webTest/Page_CURA Healthcare Service/radio_Medicare_programs'), 0)

WebUI.check(findTestObject('webTest/Page_CURA Healthcare Service/radio_Medicare_programs'))

WebUI.waitForElementPresent(findTestObject('webTest/Page_CURA Healthcare Service/radio_None_programs'), 0)

WebUI.check(findTestObject('webTest/Page_CURA Healthcare Service/radio_None_programs'))

WebUI.click(findTestObject('webTest/Page_CURA Healthcare Service/inputBoxDate'))

WebUI.click(findTestObject('webTest/Page_CURA Healthcare Service/td_12'))

WebUI.click(findTestObject('webTest/Page_CURA Healthcare Service/btnDate'))

WebUI.click(findTestObject('webTest/Page_CURA Healthcare Service/td_20'))

WebUI.setText(findTestObject('webTest/Page_CURA Healthcare Service/textarea_Comment'), 'ini dalah komen')

WebUI.click(findTestObject('webTest/Page_CURA Healthcare Service/btn_BookAppointment'))

